import { Card, Button } from 'react-bootstrap';

export default function CourseCard() {
    return(
      <Card>
        <Card.Header as="h5">Sample Course</Card.Header>
        <Card.Body>
          <Card.Text>Description:</Card.Text>
          <Card.Text>This is a sample course offering</Card.Text>
          <Card.Text>Price:</Card.Text>
          <Card.Text>PHP 40.000</Card.Text>
          <Button variant = "primary">Enroll</Button>
        </Card.Body>
      </Card>
    )
}

