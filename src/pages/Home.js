import {Container} from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import CourseCard from '../components/CourseCard';

export default function Home() {
	return (
		<>
			<Container>
				<Banner/>
				<Highlights/>
				<CourseCard/>
			</Container>
		</>
	)
}